package d13.stringOrder;

public class ZadanieStringOrderChange {

    public static void main(String[] args) {
        String string = "Kobyla ma maly bok";
        System.out.println(changeOrder(string));
    }

    public static String changeOrder(String string) {
        String newString = string.toLowerCase();
        String[] array = newString.split(" ");
        String[] newArray = new String[array.length];

        int counter = 1;
        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[array.length - counter];
            counter++;
        }
        String temp = String.join(" ", newArray);
        char first = Character.toUpperCase(temp.charAt(0));
        String result = first + temp.substring(1);
        return result;
}
}

